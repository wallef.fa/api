package com.walleftech.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiDeOfertaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiDeOfertaApplication.class, args);
	}

}
